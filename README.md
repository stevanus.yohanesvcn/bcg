## Supported Devices

- android
- min sdk 16

## Supported Features

- get all songs from itunes affiliate api
- search for songs by artist name using itunes affiliate api
- show artist name,album name, song name, and album cover in music card
- has a media controller
  - pause
  - resume
  - skip next
  - skip previous
  - seek duration with slider
- playing selected song
- have some indicator in the list item that the song is selected and being played
- have some indicator in the list item that the song is selected
- stop audio when search a new song
