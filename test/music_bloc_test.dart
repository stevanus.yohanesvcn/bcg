import 'package:MusicPlayer/blocs/music/music_bloc.dart';
import 'package:MusicPlayer/blocs/music/music_event.dart';
import 'package:MusicPlayer/blocs/music/music_state.dart';
import 'package:MusicPlayer/models/music_model.dart';
import 'package:MusicPlayer/resources/music/music_repository.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MocMusicRepository extends Mock implements MusicRepository {}

void main() {
  MocMusicRepository mocMusicRepository;

  setUp(() {
    mocMusicRepository = MocMusicRepository();
  });

  group("Testing Music Bloc", () {
    Songs songs = Songs();

    blocTest<MusicBloc, MusicState>(
      "emits [MusicLoading, MusicLoaded] states for successful",
      build: () {
        when(mocMusicRepository.getSongs(q: 'jason'))
            .thenAnswer((_) async => songs);
        return MusicBloc();
      },
      act: (bloc) async {
        bloc.add(FetchMusic(q: "jason"));
      },
      expect: () => [MusicLoading(), isA<MusicLoaded>()],
    );
  });
}
