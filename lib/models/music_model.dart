// To parse this JSON data, do
//
//     final songs = songsFromJson(jsonString);

import 'dart:convert';

Songs songsFromJson(String str) => Songs.fromJson(json.decode(str));

String songsToJson(Songs data) => json.encode(data.toJson());

class Songs {
  Songs({
    this.songs,
  });

  List<Song> songs;

  factory Songs.fromJson(Map<String, dynamic> json) => Songs(
        songs: List<Song>.from(json["results"].map((x) => Song.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(songs.map((x) => x.toJson())),
      };
}

class Song {
  Song({
    this.wrapperType,
    this.kind,
    this.artistId,
    this.collectionId,
    this.trackId,
    this.artistName,
    this.collectionName,
    this.trackName,
    this.collectionCensoredName,
    this.trackCensoredName,
    this.collectionArtistId,
    this.collectionArtistName,
    this.artistViewUrl,
    this.collectionViewUrl,
    this.trackViewUrl,
    this.previewUrl,
    this.artworkUrl30,
    this.artworkUrl60,
    this.artworkUrl100,
    this.collectionPrice,
    this.trackPrice,
    this.releaseDate,
    this.collectionExplicitness,
    this.trackExplicitness,
    this.discCount,
    this.discNumber,
    this.trackCount,
    this.trackNumber,
    this.trackTimeMillis,
    this.country,
    this.currency,
    this.primaryGenreName,
    this.isStreamable,
    this.contentAdvisoryRating,
  });

  String wrapperType;
  String kind;
  num artistId;
  num collectionId;
  num trackId;
  String artistName;
  String collectionName;
  String trackName;
  String collectionCensoredName;
  String trackCensoredName;
  num collectionArtistId;
  String collectionArtistName;
  String artistViewUrl;
  String collectionViewUrl;
  String trackViewUrl;
  String previewUrl;
  String artworkUrl30;
  String artworkUrl60;
  String artworkUrl100;
  num collectionPrice;
  num trackPrice;
  String releaseDate;
  String collectionExplicitness;
  String trackExplicitness;
  num discCount;
  num discNumber;
  num trackCount;
  num trackNumber;
  num trackTimeMillis;
  String country;
  String currency;
  String primaryGenreName;
  bool isStreamable;
  String contentAdvisoryRating;

  factory Song.fromJson(Map<String, dynamic> json) => Song(
        wrapperType: json["wrapperType"],
        kind: json["kind"],
        artistId: json["artistId"],
        collectionId: json["collectionId"],
        trackId: json["trackId"],
        artistName: json["artistName"],
        collectionName: json["collectionName"],
        trackName: json["trackName"],
        collectionCensoredName: json["collectionCensoredName"],
        trackCensoredName: json["trackCensoredName"],
        collectionArtistId: json["collectionArtistId"] == null
            ? null
            : json["collectionArtistId"],
        collectionArtistName: json["collectionArtistName"] == null
            ? null
            : json["collectionArtistName"],
        artistViewUrl: json["artistViewUrl"],
        collectionViewUrl: json["collectionViewUrl"],
        trackViewUrl: json["trackViewUrl"],
        previewUrl: json["previewUrl"],
        artworkUrl30: json["artworkUrl30"],
        artworkUrl60: json["artworkUrl60"],
        artworkUrl100: json["artworkUrl100"],
        collectionPrice:
            json["collectionPrice"] == null ? null : json["collectionPrice"],
        trackPrice: json["trackPrice"],
        releaseDate: json["releaseDate"],
        collectionExplicitness: json["collectionExplicitness"],
        trackExplicitness: json["trackExplicitness"],
        discCount: json["discCount"],
        discNumber: json["discNumber"],
        trackCount: json["trackCount"],
        trackNumber: json["trackNumber"],
        trackTimeMillis: json["trackTimeMillis"],
        country: json["country"],
        currency: json["currency"],
        primaryGenreName: json["primaryGenreName"],
        isStreamable: json["isStreamable"],
        contentAdvisoryRating: json["contentAdvisoryRating"] == null
            ? null
            : json["contentAdvisoryRating"],
      );

  Map<String, dynamic> toJson() => {
        "wrapperType": wrapperType,
        "kind": kind,
        "artistId": artistId,
        "collectionId": collectionId,
        "trackId": trackId,
        "artistName": artistName,
        "collectionName": collectionName,
        "trackName": trackName,
        "collectionCensoredName": collectionCensoredName,
        "trackCensoredName": trackCensoredName,
        "collectionArtistId":
            collectionArtistId == null ? null : collectionArtistId,
        "collectionArtistName":
            collectionArtistName == null ? null : collectionArtistName,
        "artistViewUrl": artistViewUrl,
        "collectionViewUrl": collectionViewUrl,
        "trackViewUrl": trackViewUrl,
        "previewUrl": previewUrl,
        "artworkUrl30": artworkUrl30,
        "artworkUrl60": artworkUrl60,
        "artworkUrl100": artworkUrl100,
        "collectionPrice": collectionPrice == null ? null : collectionPrice,
        "trackPrice": trackPrice,
        "releaseDate": releaseDate,
        "collectionExplicitness": collectionExplicitness,
        "trackExplicitness": trackExplicitness,
        "discCount": discCount,
        "discNumber": discNumber,
        "trackCount": trackCount,
        "trackNumber": trackNumber,
        "trackTimeMillis": trackTimeMillis,
        "country": country,
        "currency": currency,
        "primaryGenreName": primaryGenreName,
        "isStreamable": isStreamable,
        "contentAdvisoryRating":
            contentAdvisoryRating == null ? null : contentAdvisoryRating,
      };
}
