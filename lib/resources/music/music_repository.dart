import 'package:MusicPlayer/models/music_model.dart';
import 'package:MusicPlayer/resources/music/music_provider.dart';
import 'package:flutter/cupertino.dart';

class MusicRepository {
  MusicProvider musicProvider = MusicProvider();

  Future<Songs> getSongs({@required String q, int limit}) async {
    try {
      return await musicProvider.getSongs(q: q, limit: limit);
    } catch (e) {
      throw e.toString();
    }
  }
}
