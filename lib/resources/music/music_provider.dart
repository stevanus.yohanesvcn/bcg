import 'dart:convert';

import 'package:MusicPlayer/constant.dart';
import 'package:MusicPlayer/models/music_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' show Client;

class MusicProvider {
  Client client = Client();

  Future<Songs> getSongs({@required String q, int limit}) async {
    try {
      final result = await client.get(Uri.parse(
          "https://itunes.apple.com/search?media=music&country=id&attribute=artistTerm&term=$q&limit=$limit"));

      Map dataMap = json.decode(result.body);

      if (result.statusCode == 200) {
        return Songs.fromJson(dataMap);
      } else
        throw constants.api
            .validateCode(body: dataMap, code: result.statusCode);
    } catch (e) {
      throw e.toString();
    }
  }
}
