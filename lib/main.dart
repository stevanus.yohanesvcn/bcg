import 'package:MusicPlayer/blocs/audio_player/audio_player_bloc.dart';
import 'package:MusicPlayer/blocs/music/music_bloc.dart';
import 'package:MusicPlayer/pages/music_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';

class SimpleBlocDelegate extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print("event : $event");
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print("transation : $transition");
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
    print("error $error");
  }
}

void main() {
  Bloc.observer = SimpleBlocDelegate();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
            scaffoldBackgroundColor: const Color(0XFFF4F4F4)),
        home: MultiRepositoryProvider(providers: [
          RepositoryProvider(
            create: (contenxt) => MusicBloc(),
          ),
          RepositoryProvider(
            create: (contenxt) => AudioPlayerBloc(),
          ),
        ], child: MusicList()));
  }
}


// RepositoryProvider(
//         child: MusicList(),
//         create: (contenxt) => MusicBloc(),
//       ),