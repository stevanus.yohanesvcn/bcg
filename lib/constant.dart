import 'package:flutter/widgets.dart';

class Constant {
  static final Constant _constant = new Constant._internal();

  final String userKey = "user";
  final String storageStateMap = "map";
  final String urlAsset = "lib/assets/icons/";
  final String urlAssetImages = "lib/assets/images/";

  Api api = Api();
  CustomColors customColors = CustomColors();

  factory Constant() {
    return _constant;
  }

  Constant._internal();
}

class Api {
  String validateCode({int code, body}) {
    if (code == 408)
      return "Request time out";
    else if (code == 413)
      return "Request entity to large";
    else if (code != 502 && body['message'] != null)
      return body['message'].toString();
    else
      return "Something went wrong";
  }
}

class CustomColors {
  static final CustomColors _customColors = new CustomColors._internal();

  final gradiant03 = const Color(0XFF03737D);
  final gradiant14 = const Color(0XFF14373A);
  final gradiant31 = const Color(0XFF317257);
  final gradiant0E = const Color(0XFF0E3033);
  final transparent = const Color(0XFF737373);
  final whiteCA = const Color(0XFFCACACA);
  final orangeF7 = const Color(0XFFF7931D);
  final orangeFF = const Color(0XFFFFBF1B);
  final orangeE1 = const Color(0XFFE17C05);
  final black0E = const Color(0XFF0E3033);
  final grayCA = const Color(0XFFCACACA);
  final gray6E = const Color(0XFF6E6E6E);
  final gray86 = const Color(0XFF869799);
  final grayE6 = const Color(0XFFE6EAEA);
  final green5C = const Color(0XFF5CB85D);
  final green87 = const Color(0XFF87E53D);
  final redFF = const Color(0XFFFF4B4B);
  final gray002 = const Color.fromRGBO(0, 0, 0, 0.2);
  final gray006 = const Color.fromRGBO(0, 0, 0, 0.6);
  final gray0005 = const Color.fromRGBO(0, 0, 0, 00.5);
  final gray005 = const Color.fromRGBO(0, 0, 0, 0.05);
  final black006 = const Color.fromRGBO(14, 48, 51, 0.6);
  final black087 = const Color.fromRGBO(0, 0, 0, 0.87);
  final red007 = const Color.fromRGBO(255, 61, 61, 0.87);
  final orange05 = const Color.fromRGBO(247, 147, 29, 0.5);
  final gray255 = const Color.fromRGBO(255, 255, 255, 0.82).withOpacity(0.82);

  factory CustomColors() {
    return _customColors;
  }

  CustomColors._internal();
}

final constants = Constant();
