import 'package:audioplayers/audioplayers.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class AudioPlayerState extends Equatable {
  /// used to save audio player
  final AudioPlayer audioPlayer;

  /// used to track audio player state
  final PlayerState playerState;

  /// used to save the selected song from list song
  final num selectedIndex;

  /// used to save total duration song
  final num totalDuration;

  /// used to save current duration song
  final num nowDuration;

  AudioPlayerState(
      {@required this.audioPlayer,
      @required this.playerState,
      @required this.selectedIndex,
      @required this.nowDuration,
      @required this.totalDuration});

  factory AudioPlayerState.initialState() {
    return AudioPlayerState(
        audioPlayer: new AudioPlayer(mode: PlayerMode.MEDIA_PLAYER),
        playerState: null,
        selectedIndex: -1,
        nowDuration: 0,
        totalDuration: 0);
  }

  AudioPlayerState copyWith(
      {AudioPlayer audioPlayer,
      PlayerState playerState,
      num selectedIndex,
      num nowDuration,
      num totalDuration}) {
    return AudioPlayerState(
        audioPlayer: audioPlayer ?? this.audioPlayer,
        playerState: playerState ?? this.playerState,
        selectedIndex: selectedIndex ?? this.selectedIndex,
        nowDuration: nowDuration ?? this.nowDuration,
        totalDuration: totalDuration ?? this.totalDuration);
  }

  @override
  List<Object> get props =>
      [audioPlayer, playerState, selectedIndex, nowDuration, totalDuration];
}
