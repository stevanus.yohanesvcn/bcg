import 'package:audioplayers/audioplayers.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class AudioPlayerEvent extends Equatable {
  @override
  List<Object> get props => [];
}

///event used to start playing song
class StartAudio extends AudioPlayerEvent {
  final String url;
  final num selectedIndex;

  StartAudio({@required this.url, @required this.selectedIndex});

  @override
  List<Object> get props => [url];
}

///event used to play song from pause state
class PlayAudio extends AudioPlayerEvent {}

///event used to pause song
class PausedAudio extends AudioPlayerEvent {}

///event used to stop song
class StopAudio extends AudioPlayerEvent {}

//event used to skip next audio
class SkipNextAudio extends AudioPlayerEvent {
  ///url from song want to play
  final String url;

  ///selectedindex index from list song
  final num selectedIndex;

  SkipNextAudio({@required this.url, @required this.selectedIndex});

  @override
  List<Object> get props => [url, selectedIndex];
}

//event used to skip next audio
class SkipPrevAudio extends AudioPlayerEvent {
  ///url from song want to play
  final String url;

  ///selectedindex index from list song
  final num selectedIndex;

  SkipPrevAudio({@required this.url, @required this.selectedIndex});

  @override
  List<Object> get props => [url, selectedIndex];
}

//event used to change playerstate audio player
class ChangeAudioState extends AudioPlayerEvent {
  final PlayerState playerState;

  ChangeAudioState({@required this.playerState});

  @override
  List<Object> get props => [playerState];
}

//event used to set current duration
class SetNowDuration extends AudioPlayerEvent {
  final num nowDuration;

  SetNowDuration({@required this.nowDuration});

  @override
  List<Object> get props => [nowDuration];
}

//event used to set total duration
class SetTotalDuration extends AudioPlayerEvent {
  final num duration;

  SetTotalDuration({@required this.duration});

  @override
  List<Object> get props => [duration];
}

//event used to controll value from slider to seek audiplayer
class SeekAudioPlayer extends AudioPlayerEvent {
  ///total duration seek
  final num duration;

  SeekAudioPlayer({@required this.duration});

  @override
  List<Object> get props => [duration];
}
