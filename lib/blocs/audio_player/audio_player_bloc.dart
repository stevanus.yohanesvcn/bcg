import 'package:MusicPlayer/blocs/audio_player/audio_player_event.dart';
import 'package:MusicPlayer/blocs/audio_player/audio_player_state.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AudioPlayerBloc extends Bloc<AudioPlayerEvent, AudioPlayerState> {
  AudioPlayerBloc() : super(AudioPlayerState.initialState());

  @override
  Stream<AudioPlayerState> mapEventToState(AudioPlayerEvent event) async* {
    if (event is StartAudio) {
      AudioPlayer audioPlayer = state.audioPlayer;
      if (audioPlayer == null) audioPlayer = new AudioPlayer();
      audioPlayer.play(event.url);

      //listen every playerstate change
      audioPlayer.onPlayerStateChanged.listen((state) {
        add(ChangeAudioState(playerState: state));
      });

      //listen every totalduration change
      audioPlayer.onDurationChanged.listen((duration) {
        add(SetTotalDuration(duration: duration.inMicroseconds));
      });

      //listen every currentduraiton change
      audioPlayer.onAudioPositionChanged.listen((duration) {
        add(SetNowDuration(nowDuration: duration.inMicroseconds));
      });

      yield state.copyWith(
          audioPlayer: audioPlayer,
          playerState: PlayerState.PLAYING,
          selectedIndex: event.selectedIndex);
    } else if (event is ChangeAudioState) {
      yield state.copyWith(playerState: event.playerState);
    } else if (event is PlayAudio) {
      state.audioPlayer.resume();
    } else if (event is PausedAudio) {
      state.audioPlayer.pause();
    } else if (event is SkipNextAudio) {
      state.audioPlayer.play(event.url);
      yield state.copyWith(selectedIndex: event.selectedIndex);
    } else if (event is SkipPrevAudio) {
      state.audioPlayer.play(event.url);
      yield state.copyWith(selectedIndex: event.selectedIndex);
    } else if (event is SetTotalDuration) {
      yield state.copyWith(totalDuration: event.duration);
    } else if (event is SetNowDuration) {
      yield state.copyWith(nowDuration: event.nowDuration);
    } else if (event is SeekAudioPlayer) {
      Duration duration = Duration(microseconds: event.duration.toInt());
      state.audioPlayer.seek(duration);
    } else if (event is StopAudio) {
      state.audioPlayer.stop();
      yield state.copyWith(selectedIndex: -1);
    }
  }
}
