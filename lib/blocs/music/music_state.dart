import 'package:MusicPlayer/models/music_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class MusicState extends Equatable {
  @override
  List<Object> get props => [];
}

//state for loading
class MusicLoading extends MusicState {}

//state for error
class MusicFailure extends MusicState {
  final String error;

  MusicFailure({@required this.error});

  @override
  List<Object> get props => [error];
}

//state for songs loaded
class MusicLoaded extends MusicState {
  final Songs songs;

  MusicLoaded({@required this.songs});

  @override
  List<Object> get props => [songs];
}
