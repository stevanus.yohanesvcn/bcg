import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class MusicEvent extends Equatable {
  @override
  List<Object> get props => [];
}

//event used to retrieve songs data from apple api
class FetchMusic extends MusicEvent {
  final String q;

  FetchMusic({@required this.q});

  @override
  List<Object> get props => [q];
}
