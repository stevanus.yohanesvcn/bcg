import 'package:MusicPlayer/blocs/music/music_event.dart';
import 'package:MusicPlayer/blocs/music/music_state.dart';
import 'package:MusicPlayer/models/music_model.dart';
import 'package:MusicPlayer/resources/music/music_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MusicBloc extends Bloc<MusicEvent, MusicState> {
  MusicBloc() : super(MusicLoading());

  MusicRepository _musicRepository = MusicRepository();

  @override
  Stream<MusicState> mapEventToState(MusicEvent event) async* {
    if (event is FetchMusic) {
      yield MusicLoading();

      try {
        Songs songs = await _musicRepository.getSongs(q: event.q);

        yield MusicLoaded(songs: songs);
      } catch (e) {
        yield MusicFailure(error: e.toString());
      }
    }
  }
}
