import 'package:MusicPlayer/models/music_model.dart';
import 'package:flutter/material.dart';

class MusicCard extends StatefulWidget {
  final Song song;

  ///flag for indicator, if the card song is seleceted and playerstate is playing. will show the indicator
  final bool showWave;
  //flag for selected card, if the card song is selected, will change the background color
  final bool isSelected;

  MusicCard({
    @required this.song,
    @required this.showWave,
    @required this.isSelected,
  });

  @override
  _MusicCardState createState() => _MusicCardState();
}

class _MusicCardState extends State<MusicCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 110,
      color: widget.isSelected ? Color(0xffd8d8d8) : null,
      padding: EdgeInsets.all(20),
      child: Container(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image.network(
              widget.song.artworkUrl100,
              width: 100,
              height: 100,
            ),
            Flexible(
              child: Container(
                width: 300,
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Flexible(
                              child: Container(
                                child: Text(
                                  widget.song.trackName,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18,
                                      overflow: TextOverflow.ellipsis),
                                ),
                              ),
                            ),
                            Flexible(
                              child: Text(widget.song.artistName,
                                  style: TextStyle(fontSize: 14),
                                  overflow: TextOverflow.ellipsis),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Flexible(
                      child: Text(
                        widget.song.collectionName,
                        style: TextStyle(
                            fontSize: 12,
                            color: Color(0XffADADAD),
                            overflow: TextOverflow.ellipsis),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: AnimatedOpacity(
                duration: Duration(milliseconds: 100),
                opacity: widget.showWave ? 1 : 0,
                child: Container(
                  alignment: Alignment.centerRight,
                  child: Image.asset(
                    "lib/assets/wave.gif",
                    width: 100,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
