import 'package:MusicPlayer/blocs/audio_player/audio_player_bloc.dart';
import 'package:MusicPlayer/blocs/audio_player/audio_player_event.dart';
import 'package:MusicPlayer/blocs/audio_player/audio_player_state.dart';
import 'package:MusicPlayer/models/music_model.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MusicController extends StatefulWidget {
  final Songs songs;

  MusicController({@required this.songs});

  @override
  _MusicControllerState createState() => _MusicControllerState();
}

class _MusicControllerState extends State<MusicController> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AudioPlayerBloc, AudioPlayerState>(
        builder: (context, state) {
      num tempSeletedIndex = state.selectedIndex;

      return Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        height: 100,
        decoration: BoxDecoration(
          color: Color(0xffeeeeee),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.6,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      /**
                       * ketika di skip prev harus di cek apakah index -1 it lebiih kecil dari 0,
                       * jika lebih kecil dari 0 maka otomatis akan play yang terakhir
                       */

                      num index = (tempSeletedIndex - 1) < 0
                          ? widget.songs.songs.length - 1
                          : tempSeletedIndex - 1;

                      RepositoryProvider.of<AudioPlayerBloc>(context).add(
                          SkipPrevAudio(
                              selectedIndex: index,
                              url: widget.songs.songs[index].previewUrl));
                    },
                    child: Icon(
                      Icons.skip_previous,
                      size: 30,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      if (state.playerState == PlayerState.STOPPED ||
                          state.playerState == PlayerState.PAUSED ||
                          state.playerState == PlayerState.COMPLETED) {
                        RepositoryProvider.of<AudioPlayerBloc>(context)
                            .add(PlayAudio());
                      } else if (state.playerState == PlayerState.PLAYING) {
                        RepositoryProvider.of<AudioPlayerBloc>(context)
                            .add(PausedAudio());
                      }
                    },
                    child: Icon(
                      state.playerState == PlayerState.PAUSED ||
                              state.playerState == PlayerState.STOPPED ||
                              state.playerState == PlayerState.COMPLETED
                          ? Icons.play_arrow
                          : Icons.pause,
                      size: 30,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      /**
                       * ketika di skip next harus di cek apakah index +1 it lebiih besar dari array songs,
                       * jika lebih besar maka otomatis akan play yang pertama
                       */

                      num index =
                          (tempSeletedIndex + 1) > widget.songs.songs.length - 1
                              ? 0
                              : tempSeletedIndex + 1;

                      RepositoryProvider.of<AudioPlayerBloc>(context).add(
                          SkipNextAudio(
                              selectedIndex: index,
                              url: widget.songs.songs[index].previewUrl));
                    },
                    child: Icon(
                      Icons.skip_next,
                      size: 30,
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: CupertinoSlider(
                value: state.totalDuration == 0
                    ? 0
                    : state.nowDuration / state.totalDuration * 100,
                onChanged: (rating) {
                  RepositoryProvider.of<AudioPlayerBloc>(context).add(
                      SeekAudioPlayer(
                          duration: (rating * state.totalDuration / 100)));
                },
                onChangeEnd: (rating) {},
                divisions: 100,
                max: 100,
                min: 0,
              ),
            )
          ],
        ),
      );
    });
  }
}
