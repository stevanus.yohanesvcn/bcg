import 'package:MusicPlayer/blocs/audio_player/audio_player_bloc.dart';
import 'package:MusicPlayer/blocs/audio_player/audio_player_event.dart';
import 'package:MusicPlayer/blocs/audio_player/audio_player_state.dart';
import 'package:MusicPlayer/blocs/music/music_bloc.dart';
import 'package:MusicPlayer/blocs/music/music_event.dart';
import 'package:MusicPlayer/blocs/music/music_state.dart';
import 'package:MusicPlayer/widgets/music_list/music_card.dart';
import 'package:MusicPlayer/widgets/music_list/music_controller.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MusicList extends StatefulWidget {
  @override
  _MusicListState createState() => _MusicListState();
}

class _MusicListState extends State<MusicList> {
  TextEditingController _searchMusic;

  @override
  void initState() {
    _searchMusic = TextEditingController();
    RepositoryProvider.of<MusicBloc>(context)
        .add(FetchMusic(q: _searchMusic.text));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Stack(
        children: [
          SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  height: 100,
                  padding: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
                  child: TextFormField(
                    style: TextStyle(fontSize: 24, color: Colors.black),
                    textAlign: TextAlign.center,
                    textAlignVertical: TextAlignVertical.center,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.search,
                    controller: _searchMusic,
                    onFieldSubmitted: (value) {
                      RepositoryProvider.of<MusicBloc>(context)
                          .add(FetchMusic(q: _searchMusic.text));
                      /**
                       * every search submit we need to make audio stoped, 
                       * and make nowduration is 0
                       * and totalduration is 0  
                       */
                      RepositoryProvider.of<AudioPlayerBloc>(context)
                          .add(StopAudio());
                      RepositoryProvider.of<AudioPlayerBloc>(context)
                          .add(SetNowDuration(nowDuration: 0));
                      RepositoryProvider.of<AudioPlayerBloc>(context)
                          .add(SetTotalDuration(duration: 0));
                    },
                    decoration: InputDecoration(
                      hintText: "Search Artist",
                      hintStyle: TextStyle(color: Colors.black),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.blue[300], width: 1),
                      ),
                      border: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.grey[300], width: 1),
                      ),
                      fillColor: Colors.white,
                      filled: true,
                      contentPadding: EdgeInsets.all(10.0),
                    ),
                  ),
                ),
                BlocBuilder<AudioPlayerBloc, AudioPlayerState>(
                    builder: (context, playerState) {
                  return BlocBuilder<MusicBloc, MusicState>(
                    builder: (context, state) {
                      if (state is MusicLoaded) {
                        return ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                BlocProvider.of<AudioPlayerBloc>(context).add(
                                    StartAudio(
                                        url:
                                            state.songs.songs[index].previewUrl,
                                        selectedIndex: index));
                              },
                              child: MusicCard(
                                isSelected: playerState.selectedIndex == index,
                                showWave: playerState.selectedIndex == index &&
                                    playerState.playerState ==
                                        PlayerState.PLAYING,
                                song: state.songs.songs[index],
                              ),
                            );
                          },
                          itemCount: state.songs.songs.length,
                        );
                      } else if (state is MusicLoading) {
                        return CircularProgressIndicator();
                      } else
                        return Container();
                    },
                  );
                }),
                Container(
                  height: 120,
                )
              ],
            ),
          ),
          BlocBuilder<AudioPlayerBloc, AudioPlayerState>(
              builder: (context, state) {
            return BlocBuilder<MusicBloc, MusicState>(
                builder: (context, stateSongs) {
              if (stateSongs is MusicLoaded)
                return AnimatedOpacity(
                  opacity: state.playerState != null ? 1 : 0,
                  duration: Duration(milliseconds: 100),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: MusicController(
                      songs: stateSongs.songs,
                    ),
                  ),
                );

              return Container();
            });
          })
        ],
      ),
    ));
  }
}
